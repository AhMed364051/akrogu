<?php
	session_start();

	if (!$_SESSION['user']) {
		header('Location: signin.php');
	}
	require_once 'vendor/connect.php';
?> 



<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Akrogu</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"/>
    <link href="cropperjs/cropper.min.css" rel="stylesheet" type="text/css"/>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Russo+One&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="build/css/styles.css">
	<meta name="description" content="Сайт, позволяет делиться рецензиями(отзывами, комментариями) на аниме, мангу с другими пользователями" />
	<!-- Open Graph -->
	<meta property="og:title" content="Akrogu" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="http://akrogu.com" />
	<meta property="og:site_name" content="Akrogu" />
	<meta property="og:description" content="Позволь глазу Akrogu стать твоей путеводной аниме звездой!" />
	<meta property="og:image" content="img/preview.jpg">
</head>
<body>
	<div class="preloader js-hide">
		<div class="preloader__loading">
			Akrogu
		</div>
	</div>
	<header class="header">
		<div class="container">
			<div class="header__inner">
				<div class="nav">
					<a href="profile">
						<img src="img/logo.svg" alt="logo" class="logo-min">
					</a>
					<div class="player">
						<a href="https://ruv.hotmo.org/song/62967143" target="_blank" class="player__link" download>
							<span class="player__name">Наруто - Опенинг 3</span>	
						</a>
						<div class="player__btns">
							<svg class="player__box">
								<use xlink:href="#play" class="player__btn-play"></use>
							</svg>
							<button href="#" class="player__btn">Случайный опенинг</button>
						</div>
					</div>
					<div class="user btn-popup-show" data-popup="#popup-options" data-call-popup="popup">
						<div class="user__box">
							<img class="user__pic" src="<?= (!$_SESSION['user']['userpic']) ? "uploads/default.svg" : $_SESSION['user']['userpic']?>">
						</div>
						<span class="user__nick" data-call-popup="popup">
							<?= $_SESSION['user']['nickname']?>
						</span>
					</div>
				</div>
			</div>
		</div>
	</header>
	<main class="main">
		<div class="container">
			<div class="main__inner">
				<form class="about" action="/vendor/newpost.php" method="post" accept="image/jpeg, image/jpg" autocomplete="off" enctype="multipart/form-data">
					<div class="about__box about__box_title">
						<input type="text" name="list_name" value="Название аниме" class="about__name" id="nameList">
						<label for="nameList" class="about__pen" >
							<svg>
								<use xlink:href="#pen"></use>
							</svg>
						</label>
						<small class="about__warning about__warning_name">Ошибка</small>
					</div>
					<div class="about__holder">
						<label class="about__box">
							<textarea name="comment" class="about__comment" placeholder="Комментарий к аниме" id="commentList"></textarea>
							<small class="about__warning about__warning_comment">Ошибка</small>
						</label>
						<div class="about__box about__box_poster">
							<div class="about__preview">
								<input name="poster" type="file" id="filePoster" class="about__file" accept="image/jpeg, image/png">
								<div class="about__poster">
									Постер
								</div>
								<label for="filePoster" class="about__text-poster">
									
								</label>
							</div>
							<small class="about__warning about__warning_poster">Ошибка</small>
						</div>
					</div>
					<input type="text" name="user_id" value="<?=$_SESSION['user']['id_user']?>" hidden>
					<div class="about__box">
						<ul class="rating" data-total-value="0">
							<li class="rating__item" data-item-value="10">★</li>
							<li class="rating__item" data-item-value="9">★</li>
							<li class="rating__item" data-item-value="8">★</li>
							<li class="rating__item" data-item-value="7">★</li>
							<li class="rating__item" data-item-value="6">★</li>
							<li class="rating__item" data-item-value="5">★</li>
							<li class="rating__item" data-item-value="4">★</li>
							<li class="rating__item" data-item-value="3">★</li>
							<li class="rating__item" data-item-value="2">★</li>
							<li class="rating__item" data-item-value="1">★</li>
						</ul>
						<small class="about__warning about__warning_rating">Ошибка</small>
						<input type="text" name="rating" class="rating-input"  id="gradeList" hidden>
					</div>
					<button type="submit" class="button about__btn">Добавить</button>
					<div class="about__advice">
						Будь <span class="about__word about__word_adjective verb">целеустремленным</span>
						как <span class="about__word about__word_name verb">Наруто</span>
					</div>
				</form>
				<div class="list">
					<div class="list__details">
						<h2 class="list__title">
							Список:
						</h2>
						<div class="list__helpers">
							<div class="list__search" >
								<input type="text" class="list__input" id="search" placeholder="Поиск">
								<img src="img/loop.svg" alt="loop" class="list__loop">
							</div>
							<div class="list__sort">
								<button class="list__btn list__btn_asc">A-Z</button>
								<button class="list__btn list__btn_desk">Z-A</button>
								<button class="list__btn list__btn_share" data-clipboard-text="http://kotiko73.beget.tech/id<?=$_SESSION['user']['id_user']?>">
									<img src="img/clippy.svg" alt="share" title="Поделиться!">
								</button>
							</div>
						</div>
					</div>
					<ul class="list__holder">
						<?php foreach(getAll("SELECT * FROM list WHERE user_id ={$_SESSION['user']['id_user']}") as $list) {?>
							<li class="list__item" data-list-name="<?=$list['list_name']?>">
								<span class="list__name" data-call-popup="popup">
									<?=$list['list_name']?>		
								</span>
								<a class="list__about btn-popup-show" data-item-id="<?=$list['id_list']?>" data-popup="#popup-about" data-call-popup="popup"></a>
								<a href="vendor/deletelistitem.php?id=<?=$list['id_list']?>" class="list__del">
									<img src="img/eks.svg" alt="delete">
								</a>
							</li>
						<?php }?>
					</ul>
					<div class="list__warning">
						Пусто
					</div>
				</div>
			</div>
		</div>
		<div class="popup js-popup-hide btn-popup-hide" id="popup-options">
			<div class="popup__body">
				<img src="img/horn-left.svg" class="popup__horn popup__horn_left">
				<img src="img/horn-center.svg" class="popup__horn popup__horn_center">
				<img src="img/horn-right.svg" class="popup__horn popup__horn_right">
				<div class="popup__options">
					<div class="popup__eye">
						<div class="popup__eyeball"></div>
					</div>
					<div class="demon-eye">

					</div>
					<div class="teath">
						<div class="teath__item"></div>
						<div class="teath__item"></div>
						<div class="teath__item"></div>
						<div class="teath__item"></div>
						<div class="teath__item"></div>
						<div class="teath__item"></div>
					</div>
					<ul class="popup__nav">
						<li class="popup__item">
							<form class="popup__userpic" method="post" enctype="multipart/form-data">
								<label class="popup__label">
									<input type="file" name="userpic" class="popup__file" id="upload_image">
									<span class="popup__link">Обновить фотографию</span>
								</label>
							</form>
						</li>
						<li class="popup__item">
							<a href="/vendor/deluserpic.php" class="popup__link">Удалить фотографию</a>
						</li>
						<li class="popup__item">
							<a href="/vendor/logout.php" class="popup__link">Выйти</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="popup js-popup-hide btn-popup-hide" id="popup-about">
			<div class="popup__body btn-popup-hide">
				<div class="popup__content">
					<button class="popup__close btn-popup-hide">
						<img src="img/eks.svg" alt="delete" class="btn-popup-hide">
					</button>
					<div class="popup__row">
						<h1 class="popup__title">
							Название...
						</h1>
						<time class="popup__time">
							Время публикации...
						</time>
					</div>
					<div class="popup__row">
						<div class="popup__left">
							<p class="popup__text">
								Комментарий...
							</p>
							<span class="popup__grade">
								Оценка...
							</span>
						</div>	
						<div class="popup__right">
							<img src="" alt="poster" class="popup__poster">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="img-container">
                            <div class="row">
                                <div class="col-md-12">  
                                    <img id="image">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
						<button type="button" id="crop">Продолжить</button>
                        <button type="button" data-dismiss="modal">Отменить</button>
                    </div>
                </div>
            </div>
		</div>
	</main>
	<ul class="animation-bg">
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
	</ul>
	<svg display="none">
		<symbol id="play" width="17" height="19" viewBox="0 0 17 19" xmlns="http://www.w3.org/2000/svg">
			<path d="M16 7.76795C17.3333 8.53775 17.3333 10.4623 16 11.2321L3.25 18.5933C1.91667 19.3631 0.249999 18.4008 0.249999 16.8612L0.249999 2.13878C0.249999 0.599184 1.91667 -0.363067 3.25 0.406733L16 7.76795Z"/>
		</symbol>
		<symbol id="pause" width="19" height="19" viewBox="0 0 19 21" xmlns="http://www.w3.org/2000/svg">
			<rect y="2" width="8" height="18" rx="2"/>
			<rect y="2" width="20" height="18" rx="2" fill="transparent"/>
			<rect x="11" y="2" width="8" height="18" rx="2"/>
		</symbol>
		<symbol id="pen" width="27" height="27" viewBox="0 0 27 27">
			<path d="M23.2428 1.85405C21.882 0.495591 19.6782 0.495591 18.3174 1.85405L17.0844 3.09374L3.9599 16.2113L3.93201 16.2394C3.92525 16.2462 3.92525 16.2534 3.91806 16.2534C3.90411 16.2743 3.8832 16.295 3.86947 16.3159C3.86947 16.3229 3.86228 16.3229 3.86228 16.3299C3.84833 16.3508 3.84157 16.3647 3.82741 16.3856C3.82065 16.3926 3.82065 16.3994 3.81368 16.4066C3.80671 16.4275 3.79974 16.4414 3.79254 16.4623C3.79254 16.4691 3.78579 16.4691 3.78579 16.4763L0.873849 25.2328C0.788428 25.4821 0.853366 25.7582 1.04099 25.9432C1.17282 26.0733 1.35064 26.1461 1.53564 26.1454C1.61126 26.1441 1.68622 26.1323 1.75857 26.1106L10.5083 23.1916C10.5151 23.1916 10.5151 23.1916 10.5223 23.1849C10.5443 23.1784 10.5654 23.169 10.5848 23.1568C10.5903 23.1561 10.5951 23.1537 10.599 23.15C10.6197 23.1361 10.6476 23.1219 10.6685 23.108C10.6892 23.0942 10.7103 23.0733 10.7313 23.0594C10.7382 23.0522 10.745 23.0522 10.745 23.0454C10.7522 23.0385 10.7661 23.0317 10.7731 23.0175L25.1306 8.66005C26.489 7.2992 26.489 5.09547 25.1306 3.73483L23.2428 1.85405ZM10.2854 21.5545L5.4369 16.7062L17.5721 4.57096L22.4206 9.41925L10.2854 21.5545ZM4.75397 17.9949L8.98971 22.2304L2.62934 24.3481L4.75397 17.9949ZM24.1484 7.68468L23.4099 8.43015L18.5612 3.58142L19.3069 2.83617C20.1225 2.02118 21.4446 2.02118 22.2605 2.83617L24.1552 4.7309C24.9647 5.55025 24.9617 6.86926 24.1484 7.68468Z"/>
		</symbol>
	</svg>
	<script src="build/js/main.js"></script>
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js"></script> 
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>
	<script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.min.js"></script>
	<script src="https://unpkg.com/tippy.js@6/dist/tippy-bundle.umd.js"></script>
	<script src="cropperjs/cropper.min.js" type="text/javascript"></script>
	<script>

		const clipboard = new ClipboardJS('.list__btn_share');

		tippy('.list__btn_share', {
			hideOnClick: false,
			trigger: 'click',
			content: "Скопировано!",
			placement: 'top-end',
			onShow(instance) {
				setTimeout(() => {
				instance.hide();
				}, 2000);
			},
		});

		$(document).ready(function(){

			var $modal = $('#modal');

			var image = document.getElementById('image');

			var cropper;

			$('.popup__file').change(function(event){
				var files = event.target.files;

				var done = function(url){
					image.src = url;
					document.querySelector('.popup').classList.add("js-popup-hide");
					document.querySelector('body').classList.remove("fixed");
					$modal.modal('show');
				};

				if(files && files.length > 0)
				{
					reader = new FileReader();
					reader.onload = function(event)
					{
						done(reader.result);
						document.querySelector('.popup__file').value = "";	
					};
					reader.readAsDataURL(files[0]);
				}
			});

			$modal.on('shown.bs.modal', function() {
				cropper = new Cropper(image, {
					aspectRatio: 1,
					viewMode: 1,
				});
			}).on('hidden.bs.modal', function(){
				cropper.destroy();
				cropper = null;
			});

			$('#crop').click(function(){
				canvas = cropper.getCroppedCanvas({
					width:400,
					height:400
				});

				canvas.toBlob(function(blob){
					url = URL.createObjectURL(blob);
					var reader = new FileReader();
					reader.readAsDataURL(blob);
					reader.onloadend = function(){
						var base64data = reader.result;
						$.ajax({
							url:'vendor/edituserpic.php',
							method:'POST',
							data:{image:base64data},
							success:function(data)
							{
								$modal.modal('hide');
								$('.user__pic').attr('src', base64data);
							}
						});
					};
				});
			});
		});
	</script>
</body>
</html>