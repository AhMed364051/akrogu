<?php
	session_start();
	require_once 'connect.php';

	$login = $_POST['login'];
	$password = $_POST['password'];

	$hashPassword = mysqli_fetch_assoc(mysqli_query($connect, "SELECT `password` FROM `users` WHERE `login` = '$login'"));
	$hash = $hashPassword['password'];	

	if (password_verify($password, $hash)) {
		$check_user = mysqli_query($connect, "SELECT * FROM `users` WHERE `login` = '$login' AND `password` = '$hash'");

		if (mysqli_num_rows($check_user) > 0) {

			$user = mysqli_fetch_assoc($check_user);
	
			$_SESSION['user'] = [
				"id_user" => $user['id_user'],
				"nickname" => $user['nickname'],
				"login" => $user['login'],
				"userpic" =>  $user['userpic']
			];
	
			header('Location: ../profile');
		}
	}
	else {
		$_SESSION['msg'] = "Неверный логин или пароль!";
		header('Location: ../signin.php');
	}
