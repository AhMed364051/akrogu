<?php
    header('Content-Type: application/json');
	session_start();
	require_once 'connect.php';

    $listId = $_GET['id'];

    

    $res = mysqli_fetch_assoc(mysqli_query($connect, "SELECT `list_name`, `comment`, `rating`, `poster`, `date` FROM `list` WHERE `id_list` = '$listId'"));
        
    
    $monthes = array('янв', 'фев', 'мар', 'апр', 'мая', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек');
    $numberMonth = (int)date('m', strtotime($res['date'])) - 1;
    $day = (int)date('d', strtotime($res['date']));
	$res['date'] = date('Опубликовано ' . $day . ' ' . $monthes[$numberMonth] . '  Y в H:i' , strtotime($res['date']));

    $_SESSION['res'] = [
        "listName" => $res['list_name'],
        "comment" => $res['comment'],
        "rating" => $res['rating'],
        "poster" => $res['poster'],
        "date" => $res['date']
    ];

    header('Content-Type: text/json; charset=utf-8');
    
    $data = json_encode($_SESSION['res']);  
    echo $data;

    
