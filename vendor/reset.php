<?php
    session_start();
    require_once 'connect.php';

    function utf8mail($to,$s,$body) {
        $from_name='Сброс пароля на сайте рецензий на аниме "Akrogu"';
        $from_a = "akrogu@gmail.com";
        $reply="akrogu@gmail.com";
        $s= "=?utf-8?b?".base64_encode($s)."?=";
        $headers = "MIME-Version: 1.0\r\n";
        $headers.= "From: =?utf-8?b?".base64_encode($from_name)."?= <".$from_a.">\r\n";
        $headers.= "Content-Type: text/plain;charset=utf-8\r\n";
        $headers.= "Reply-To: $reply\r\n";  
        $headers.= "X-Mailer: PHP/" . phpversion();
        mail($to, $s, $body, $headers);
    }

    if (isset($_POST['login'])) {
        
        $row = mysqli_fetch_assoc(mysqli_query($connect, "SELECT `login` FROM `users` WHERE `login` = '{$_POST['login']}'"));
        $code = rand(1000000, 9999999);
        
        utf8mail($row['login'], 'Сброс пароля на сайте рецензий на аниме "Akrogu"', 'Ваш код для сброса пароля - ' . $code);

        mysqli_query($connect, "UPDATE `users` SET `code` = '$code'  WHERE `login` = '{$_POST['login']}'");
        $_SESSION['msg'] = "Код был выслан вам на почту!";
		header('Location: ../resetPassword.php');
    }