<?php
	session_start();
	require_once 'vendor/connect.php';

	$idUser = $_GET['id_user'];

	$about = mysqli_fetch_assoc(mysqli_query($connect, "SELECT `id_user`, `nickname`, `userpic` FROM `users` WHERE id_user = '$idUser'"));

	if (!$about) {
		header('Location: ./error');
	}

	$_SESSION['about'] = [
		"id_user" => $about['id_user'],
		"nickname" => $about['nickname'],
		"userpic" =>  $about['userpic']
	];

?> 

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Akrogu</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Russo+One&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="build/css/styles.css">
	<meta name="description" content="Сайт, позволяет делиться рецензиями(отзывами, комментариями) на аниме, мангу с другими пользователями" />
	<!-- Open Graph -->
	<meta property="og:title" content="Akrogu веб-сайт для аниме рецензий" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="http://akrogu.com" />
	<meta property="og:site_name" content="Akrogu" />
	<meta property="og:description" content="Позволь глазу Akrogu стать твоей путеводной аниме звездой!" />
	<meta property="og:image" content="img/preview.jpg">
</head>
<body>
	<div class="preloader js-hide">
		<div class="preloader__loading">
			Akrogu
		</div>
	</div>
	<header class="header">
		<div class="container">
			<div class="header__inner">
				<div class="nav">
					<a href="profile">
						<img src="img/logo.svg" alt="logo" class="logo-min">
					</a>
					<div class="player">
						<a href="https://ruv.hotmo.org/song/62967143" target="_blank" class="player__link" download>
							<span class="player__name">Наруто - Опенинг 3</span>	
						</a>
						<div class="player__btns">
							<svg class="player__box">
								<use xlink:href="#play" class="player__btn-play"></use>
							</svg>
							<button href="#" class="player__btn">Случайный опенинг</button>
						</div>
					</div>
					<div class="user user-result btn-popup" data-popup="#popup-options" data-call-popup="popup">
						<div class="user__box">
							<img class="user__pic" src="/<?= (!$_SESSION['about']['userpic']) ? "uploads/default.svg" : $_SESSION['about']['userpic']?>" alt="user_pic">
						</div>
						<span class="user__nick">
							<?= $_SESSION['about']['nickname']?>
						</span>
					</div>
				</div>
			</div>
		</div>
	</header>
    <main class="result">
        <div class="container">
            <div class="result__inner">
                <div class="list list-result">
					<div class="list__details">
						<h2 class="list__title">
							Список:
						</h2>
						<div class="list__helpers">
							<div class="list__search" >
								<input type="text" class="list__input" id="search" placeholder="Поиск">
								<img src="img/loop.svg" alt="loop" class="list__loop">
							</div>
							<div class="list__sort">
								<button class="list__btn list__btn_asc">A-Z</button>
								<button class="list__btn list__btn_desk">Z-A</button>
								<button class="list__btn list__btn_share" data-clipboard-text="http://kotiko73.beget.tech/id<?=$_SESSION['about']['id_user']?>">
									<img src="img/clippy.svg" alt="share" title="Поделиться!">
								</button>
							</div>
						</div>
					</div>
					<ul class="list__holder list__holder-result">
						<?php foreach(getAll("SELECT * FROM list WHERE user_id ={$_SESSION['about']['id_user']}") as $list) {?>
							<li class="list__item" data-list-name="<?=$list['list_name']?>">
								<span class="list__name text-center" data-call-popup="popup">
									<?=$list['list_name']?>	
								</span>
								<a class="list__about btn-popup-show" data-item-id="<?=$list['id_list']?>" data-popup="#popup-about" data-call-popup="popup"></a>
							</li>
						<?php }?>
					</ul>
					<div class="list__warning">
						Пусто
					</div>	
				</div>
            </div>
        </div>
	</main>
	<div class="popup js-popup-hide btn-popup-hide" id="popup-about">
		<div class="popup__body btn-popup-hide">
			<div class="popup__content">
				<button class="popup__close btn-popup-hide">
					<img src="img/eks.svg" alt="delete" class="btn-popup-hide">
				</button>
				<div class="popup__row">
					<h1 class="popup__title">
						Название...
					</h1>
					<time class="popup__time">
						Время публикации...
					</time>
				</div>
				<div class="popup__row">
					<div class="popup__left">
						<p class="popup__text">
							Комментарий...
						</p>
						<span class="popup__grade">
							Оценка...
						</span>
					</div>	
					<div class="popup__right">
						<img src="" alt="poster" class="popup__poster">
					</div>
				</div>
			</div>
		</div>
	</div>
	<ul class="animation-bg">
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
	</ul>
	<svg display="none">
		<symbol id="play" width="17" height="19" viewBox="0 0 17 19" xmlns="http://www.w3.org/2000/svg">
			<path d="M16 7.76795C17.3333 8.53775 17.3333 10.4623 16 11.2321L3.25 18.5933C1.91667 19.3631 0.249999 18.4008 0.249999 16.8612L0.249999 2.13878C0.249999 0.599184 1.91667 -0.363067 3.25 0.406733L16 7.76795Z"/>
		</symbol>
		<symbol id="pause" width="19" height="19" viewBox="0 0 19 21" xmlns="http://www.w3.org/2000/svg">
			<rect y="2" width="8" height="18" rx="2"/>
			<rect y="2" width="20" height="18" rx="2" fill="transparent"/>
			<rect x="11" y="2" width="8" height="18" rx="2"/>
		</symbol>
	</svg>
	<script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>
	<script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.min.js"></script>
	<script src="https://unpkg.com/tippy.js@6/dist/tippy-bundle.umd.js"></script>
	<script src="/build/js/result.js"></script>
	<script>
		const clipboard = new ClipboardJS('.list__btn_share');

		tippy('.list__btn_share', {
			hideOnClick: false,
			trigger: 'click',
			content: "Скопировано!",
			placement: 'top-end',
			onShow(instance) {
				setTimeout(() => {
				instance.hide();
				}, 2000);
			},
		});
	</script>
<body>
</html>