<?php
	session_start();

	if ($_SESSION['user']) {
		header('Location: ../profile.php');
	}
?>


<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Akrogu</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="build/css/styles.css">
	<meta name="description" content="Сайт, позволяет делиться рецензиями(отзывами, комментариями) на аниме, мангу с другими пользователями" />
	<!-- Open Graph -->
	<meta property="og:title" content="Akrogu" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="http://akrogu.com" />
	<meta property="og:site_name" content="Akrogu" />
	<meta property="og:description" content="Позволь глазу Akrogu стать твоей путеводной аниме звездой!" />
	<meta property="og:image" content="img/preview.jpg">
</head>
<body>
	<form action="/vendor/resetPassword.php" method="post" class="form" id="form" autocomplete="off">
		<div class="container">
			<div class="form__inner">
				<div class="logo">
					<ul class="rainbow">
						<li class="rainbow__item"></li>
						<li class="rainbow__item"></li>
						<li class="rainbow__item"></li>
						<li class="rainbow__item"></li>
						<li class="rainbow__item"></li>
						<li class="rainbow__item"></li>
						<li class="rainbow__item"></li>
						<li class="rainbow__item"></li>
					</ul>
					<div class="logo__horn logo__horn_left">
						<div class="logo__block logo__block_left"></div>
						<div class="logo__triangle logo__triangle_left"></div>
						<div class="logo__triangle-second logo__triangle-second_left"></div>
					</div>
					<div class="logo__horn logo__horn_center">
						<div class="logo__block logo__block_center"></div>
						<div class="logo__triangle logo__triangle_center"></div>
						<div class="logo__triangle-second logo__triangle-second_center"></div>
					</div>
					<div class="logo__horn logo__horn_right">
						<div class="logo__block logo__block_right"></div>
						<div class="logo__triangle logo__triangle_right"></div>
						<div class="logo__triangle-second logo__triangle-second_right"></div>
					</div>
					<div class="logo__head">
						<div class="logo__eye">
							<div class="logo__eyeball">
								<div class="logo__patch">
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form__data">
					<label class="form__item">
						<input type="text" class="form__input" name="login" placeholder="Введите email" id="email">
						<i class="fa fa-check-circle form__icon form__icon_success"></i>
						<i class="fa fa-exclamation-circle form__icon form__icon_error"></i>
						<small class="form__warning">Ошибка</small>
					</label>
					<label class="form__item">
						<input type="text" class="form__input" name="code" placeholder="Введите код" id="code">
						<i class="fa fa-check-circle form__icon form__icon_success"></i>
						<i class="fa fa-exclamation-circle form__icon form__icon_error"></i>
						<small class="form__warning">Ошибка</small>
					</label>
					<label class="form__item">
						<input type="password" class="form__input" name="password" placeholder="Введите новый пароль" id="password">
						<i class="fa fa-check-circle form__icon form__icon_success"></i>
						<i class="fa fa-exclamation-circle form__icon form__icon_error"></i>
						<small class="form__warning">Ошибка</small>
					</label>
					<button type="submit" class="form__btn button">Сменить пароль</button>
					<span class="form__link">
						У вас есть аккаунт? -
						<a href="./login">авторизируйтесь</a>
					</span>
					<?php if ($_SESSION['msg']) {?>
						<p class="msg">
							<?php echo $_SESSION['msg'];?>
							<?php unset($_SESSION['msg']);?>
						</p>
					<?php }?>
				</div>
			</div>
		</div>
	</form>
	<ul class="animation-bg">
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
	</ul>
	<script src="https://use.fontawesome.com/41c73f620f.js"></script>
	<script>
		document.querySelector('body').сlassList.remove("load");
	</script>
</body>
</html>
