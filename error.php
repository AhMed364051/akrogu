<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Akrogu</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="build/css/styles.css">
</head>
<body>
    <main class="error">
        <div class="container">
            <div class="error__inner">
                <h1 class="error__name">404</h1>
            </div>
        </div>
    </main>
    <ul class="animation-bg">
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
		<li class="animation-bg__item"></li>
	</ul>
</body>
</html>