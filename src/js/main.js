window.onload = function () {
	const openings = [
		{
			name: "Наруто - Опенинг 3",
			path: "naruto-op3.mp3"
		},
		{
			name: "Клинок рассекающий демонов - Опенинг 1",
			path: "blade-slayer-op1.mp3"
		},
		{
			name: "Атака титанов - Опенинг 1",
			path: "attack-on-titan-op1.mp3"
		},
		{
			name: "Твоё имя - Опенинг 1",
			path: "your-name.mp3"
		},
		{
			name: "Обещанный Неверленд - Опенинг 1",
			path: "promised-neverland-op1.mp3"
		},
		{
			name: "Город, в котором меня нет - Опенинг 1",
			path: "erased.mp3"
		},
	];
	
	const adjectives = [
		'целеустремленным',
		'проницательным',
		'сострадательным',
		'бесстрашным',
		'чистоплотным',
		'cмышленым'
	];
	
	const names = [
		'Наруто',
		'Изуко',
		'Танджиро',
		'Гатс',
		'Леви',
		'Эл'
	];    

	// Над функцией popup и sort надо еще поработать
	// Рандомное число
	const getRandomNumber = (min, max) => Math.floor(Math.random() * (max - min)) + min;


	// Вставить после
	const insertAfter = (elem, refElem)  => refElem.parentNode.insertBefore(elem, refElem.nextSibling);

	// Плеер
	function player () {
		const btnMusic = document.querySelector(".player__btn-play");
		const btnRandomMusic = document.querySelector(".player__btn");
		const musicName = document.querySelector(".player__name");
		const musicLink = document.querySelector(".player__link");

		let lastRandomNum;
		let currentRandomNum;

		const audio = new Audio(); 

		btnMusic.addEventListener("click", function(){
			btnMusic.classList.toggle("player__btn-play_active");

			if (btnMusic.classList.contains("player__btn-play_active")) {
				playPlayer();
			}
			else {
				pausePlayer();
			}
		});

		btnRandomMusic.addEventListener("click", function(e){
			e.preventDefault();

			btnMusic.classList.add("player__btn-play_active");

			startStatePlayer();

			playPlayer();
		});

		audio.addEventListener("ended", function(){
			btnMusic.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#play");
			btnMusic.classList.remove("player__btn-play_active");
		});

		startStatePlayer();

		function startStatePlayer () {
			currentRandomNum = getRandomNumber(0, openings.length);

			if (currentRandomNum === lastRandomNum) {
				startStatePlayer();
			}

			lastRandomNum = currentRandomNum;

			musicLink.href = "openings/" + openings[currentRandomNum]["path"];
			musicName.innerHTML = openings[currentRandomNum]["name"];
			audio.src = "openings/" + openings[currentRandomNum]["path"];
		}

		function playPlayer () {
			btnMusic.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#pause");
			audio.play();
		}

		function pausePlayer () {
			btnMusic.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#play");
			audio.pause();	
		}
	}

	// запрет перетаскивания картинок
	function preventDragImg () {
		document.querySelectorAll("img").forEach(img => {
			img.addEventListener("dragstart", function(e){
				e.preventDefault();
			});
		});
	}

	// Текст-ротейтор
	function rotateText () {
		
	  	function animateWords (words, path) {
	  
			const word = document.querySelector(path);
			let duration = 4000;
			let i = 0;
	  
			function toggleWord (duration) { 
			setInterval(function(){
				
				i++;
		
				if (i === words.length) {
					i = 0;
				}

				word.innerHTML = words[i];

				word.addEventListener("animationend", function (){
					word.className = "verb";
				});
		
				word.classList.add("js-fade-in-verb");


			}, duration);
		};
	  
		toggleWord(duration);
	  };
	  
	  animateWords(adjectives, ".about__word_adjective");
	  animateWords(names, ".about__word_name");
	}

	// Оценка
	function passGrade () {
		const ratingItems = document.querySelectorAll(".rating__item");
		const ratingValue = document.querySelector(".rating-input");
		
		ratingItems.forEach(item => item.addEventListener("click", function() {
			item.parentNode.dataset.totalValue = item.dataset.itemValue;
			ratingValue.value = item.dataset.itemValue;
			
			localStorage.setItem("rating", item.parentNode.dataset.totalValue);
		}));

		ratingValue.value = localStorage.getItem("rating");

		ratingItems[0].parentNode.dataset.totalValue = localStorage.getItem("rating");
	}

	// Состояние списка
	function changeStateList () {
		const listItems = document.querySelectorAll(".list__item");
		let listTools = document.querySelector(".list__details");
		let listWarnig = document.querySelector(".list__warning");
		
		if (listItems.length === 0) {
			listTools.style.visibility = "hidden";
			listWarnig.style.display = "block";
		}
		else {
			listTools.style.visibility = "visible";
			listWarnig.style.display = "none";
		}
	}

	changeStateList ();

	function checkReview () {
		const form = document.querySelector(".about");
		const name = document.querySelector(".about__name");
		const comment = document.querySelector(".about__comment");
		const rating = document.querySelector(".rating-input");
		
		let errors;

		form.addEventListener("submit", function(e) {
			e.preventDefault();
			
			errors = 0;

			checkInputs();
		});

		function uploadPoster () {
			const reader = new FileReader();
	
			let file = document.querySelector(".about__file");
			let poster = document.querySelector(".about__poster");
	
			file.addEventListener("change", function (){
	
				let fileInfo = file.files[0];
	
				if (fileInfo.size > 2 * 1024 * 1024) {
					setErrorFor(".about__warning_poster", "&le;2 мб!");
					file.value = "";
					return;
				}
	
				if (!["image/jpeg", "image/png"].includes(fileInfo.type)) {
					setErrorFor(".about__warning_poster", "jpg или png");
					file.value = "";
					return;
				}
	
				reader.readAsDataURL(fileInfo);
	
				reader.onload = function (e) {
					localStorage.setItem("poster", `<img src="${e.target.result}">`);
					setSuccessFor(".about__warning_name");
					setPreview ();
				}
	
				reader.onerror = function (e) {
					alert("Ошибка!");
				}
			});
	
			if (localStorage.poster) {
				setPreview();
			}
	
			function setPreview () {
				poster.innerHTML = localStorage.getItem("poster");
			}
		}


		function checkInputs() {
			const nameValue = name.value.trim();
			const commentValue = comment.value.replace(/\s+/g, ' ');
			const ratingValue = rating.value.trim();
			const posterValue = document.querySelector(".about__file").value;
			localStorage.setItem("posterSrc", posterValue);

			
			
			if(!nameValue) {
				setErrorFor(".about__warning_name", "Поле название аниме - пустое!");
			}
			else if (nameValue === "Название аниме") {
				setErrorFor(".about__warning_name", "Вы не ввели название аниме!");
			}
			else if (nameValue.length < 3) {
				setErrorFor(".about__warning_name", "Меньше 3 символов!");
			}
			else {
				setSuccessFor(".about__warning_name");
			}

			if(!commentValue) {
				setErrorFor(".about__warning_comment", "Поле комментарий - пустое!");
			}
			else {
				setSuccessFor(".about__warning_comment");
			}

			if (!ratingValue) {
				setErrorFor(".about__warning_rating", "Не стоит оценка!");
			}
			else {
				setSuccessFor(".about__warning_rating");
			}

			if (!posterValue) {
				setErrorFor(".about__warning_poster", "Нет постера!");
			}
			else {
				setSuccessFor(".about__warning_poster");
			}

			if (!errors) {
				localStorage.clear();
				form.submit();
			}
		}
		
		function setErrorFor(nameWarning, message) {
			errors++;
			
			const warning = document.querySelector(`${nameWarning}`);
			warning.innerHTML = message;
			warning.classList.add("js-warning-visible");
		}

		function setSuccessFor(nameWarning) {
			const warning = document.querySelector(`${nameWarning}`);
			warning.classList.remove("js-warning-visible");
		}
		
		uploadPoster();
    }

	// Поиск в списке
	function searchMatches () {
		const input = document.querySelector("#search");
		const listNames = document.querySelectorAll(".list__name");
		const warning = document.querySelector(".list__warning");

		const regexp = /\s+/g;

		input.addEventListener("input", function (){
			let inputVal = this.value;
	
			let mismatches = 0;

			if (inputVal) {
				listNames.forEach(listName => {
					let item = listName.innerText.replace(regexp, "").toLowerCase();
					let match = inputVal.replace(regexp, "").toLowerCase();

					if (item.search(match) === -1) {
						listName.parentNode.classList.add("js-hide");
						mismatches++;

						if (mismatches === listNames.length) {
							warning.style.display = "block";
						}
					}
					else {
						listName.parentNode.classList.remove("js-hide");
						warning.style.display = "none";
					}
					
				});
			}
			else {
				listNames.forEach(itemList => {
					itemList.parentNode.classList.remove("js-hide");
					warning.style.display = "none";
				});
			}
		})
	}

	function sortList () {
		document.querySelector(".list__btn_asc").addEventListener("click", function (){
			sortListAscAndDesk ("desk");
		});

		document.querySelector(".list__btn_desk ").addEventListener("click", function (){
			sortListAscAndDesk ();
		});

		function sortListAscAndDesk (sort) {
			
			const list = document.querySelector(".list__holder");
	
			for (let i = 0; i < list.children.length; i++) {
				for (let j = i; j < list.children.length; j++) {

					let firstItem = list.children[i].dataset.listName.toLowerCase();
					let secondItem = list.children[j].dataset.listName.toLowerCase();

					if (sort === "desk" ? firstItem > secondItem : firstItem < secondItem) {
						let replacedNode = list.replaceChild(list.children[j], list.children[i]);
						insertAfter(replacedNode, list.children[i]);
					}
				}
			}
		}
	}

	// Показать модальное окно
	function popup () {
		const popupBtnsShow =  document.querySelectorAll(".btn-popup-show");
		const popupBtnsHide = document.querySelectorAll(".btn-popup-hide");
		const popups = document.querySelectorAll(".popup");


		popupBtnsShow.forEach(btnShow => {
			btnShow.addEventListener("click", function (e) { 
				if (e.target.dataset.callPopup === "popup") {
					document.querySelector(`${this.dataset.popup}`).classList.remove("js-popup-hide");
					document.body.classList.add('fixed');
				}
			});
		})
	
		popupBtnsHide.forEach(btnHide => {
			btnHide.addEventListener("click", function(e){
				if (e.target.classList.contains("btn-popup-hide")) {
					popups.forEach(popup => {
						popup.classList.add("js-popup-hide");
						document.body.classList.remove('fixed');
					});
				}
			})
		})
	}


	function listStorageData () {
		const commentList = document.querySelector("#commentList");

		putValueInLocalStorage (commentList);
	}

	// Хранит некие данные в локальном хранилище
	function putValueInLocalStorage (nameItem) {
		nameItem.addEventListener("input", function(){
			localStorage.setItem(nameItem.id, nameItem.value);
		});

		nameItem.value = localStorage.getItem(nameItem.id);
	}

	// Получить информацию о рецензиях
	function getListItems () {
		document.querySelectorAll('.list__about').forEach(item => {
			item.addEventListener("click", function (e) {
			e.preventDefault();

			const preloader = document.querySelector(".preloader").classList;

			preloader.remove("js-hide");

			fetch(`vendor/post.php?id=${this.dataset.itemId}`)
				.then(response => response.json())
				.then(data => {
					
						const {listName, comment, rating, poster, date} = data;

						document.querySelector(".popup__title").innerHTML = listName;
						document.querySelector(".popup__text").innerHTML = marked(comment);
						document.querySelector(".popup__grade").innerText = rating;
						document.querySelector(".popup__poster").src = poster;
						document.querySelector(".popup__time").innerText = date;

						setTimeout(() => {
							preloader.add("js-hide");
						}, 5000);
				});
			}); 
		});
	}

	listStorageData ();

	// Название аниме
	function behaviorNameAnime() {
		const title = document.querySelector(".about__name");

		let buffer = [];
		buffer = document.createElement('div');
		buffer.className = "buffer";
		
		insertAfter(buffer, title);

		putValueInLocalStorage (title);
		changeTitleWidth ();

		emptyTitleState ();
		
		title.addEventListener("input", changeTitleWidth);

		title.addEventListener("blur", emptyTitleState);

		title.addEventListener("focus", function (){
			if (title.value === "Название аниме") {
				title.value = "";
				changeTitleWidth();	
			}
		});

		function changeTitleWidth () {
			title.nextElementSibling.innerHTML = title.value;
			title.style.width = title.nextElementSibling.clientWidth + 'px';
		}

		function emptyTitleState () {
			if (!title.value) {
				title.value = "Название аниме";
				changeTitleWidth();
			}
		}
	}

	player();
	popup();
	preventDragImg();
	behaviorNameAnime();
	passGrade();
	rotateText();
	searchMatches();
	sortList();
	checkReview();
	getListItems(); 
}

// рефакторинг кода необходим