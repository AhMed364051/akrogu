window.onload = function () {
    function checkForm () {
		const form = document.querySelector("#form");
		const email = document.querySelector("#email");
		const password = document.querySelector("#password");
		let errors;

		form.addEventListener("submit", function(e) {
			e.preventDefault();

			errors = 0;
			
			checkInputs();
		});

		function checkInputs() {
			const emailValue = email.value.trim();
			const passwordValue = password.value.trim();
			
			if(!emailValue) {
				setErrorFor(email, "Поле email пустое");
			} 
			else if (!isEmail(emailValue)) {
				setErrorFor(email, "Неверно введен email");
			} 
			else {
				setSuccessFor(email);
			}
			
			if(!passwordValue) {
				setErrorFor(password, "Поле пароль пустое");
			}
			else {
				setSuccessFor(password);
			}
			
			if (!errors) {
				form.submit();
			}
		}
		
		function setErrorFor(input, message) {
			errors++;

			const formControl = input.parentElement;
			
			const small = formControl.querySelector("small");
			formControl.className = "form__item error";
			small.innerText = message;	
		}
		
		function setSuccessFor(input) {
			const formControl = input.parentElement;
			formControl.className = "form__item";
		}
			
		const isEmail = (email) => {
			return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
		}
    }

    checkForm ();
}