window.onload = function () {
    function checkForm () {
		const form = document.querySelector("#form");
		const username = document.querySelector("#username");
		const email = document.querySelector("#email");
		const password = document.querySelector("#password");
		const password2 = document.querySelector("#password2");
		let errors;

		form.addEventListener("submit", function(e) {
			e.preventDefault();

			errors = 0;
			
			checkInputs();
		});

		function checkInputs() {
			
			const usernameValue = username.value.trim();
			const emailValue = email.value.trim();
			const passwordValue = password.value.trim();
			const password2Value = password2.value.trim();
			
			if(!usernameValue) {
				setErrorFor(username, "Поле псевдоним пустое");
			}
			else if (usernameValue.length < 3  || usernameValue.length > 8) {
				setErrorFor(username, "Меньше 3 символов или больше 8");
			}
			else {
				setSuccessFor(username);
			}
			
			if(!emailValue) {
				setErrorFor(email, "Поле email пустое");
			} 
			else if (!isEmail(emailValue)) {
				setErrorFor(email, "Неверно введен email");
			} 
			else {
				setSuccessFor(email);
			}
			
			if(!passwordValue) {
				setErrorFor(password, "Поле пароль пустое");
			}
			else if (!isPassword(passwordValue)) {
				setErrorFor(password, "От 15 до 25 символов");
			}
			else {
				setSuccessFor(password);
			}
			
			if(!password2Value) {
				setErrorFor(password2, "Второе поле пароль пустое");
			}
			else if(passwordValue !== password2Value) {
				setErrorFor(password2, "Пароли не соответсвуют");
			}
			else{
				setSuccessFor(password2);
			}

			if (!errors) {
				form.submit();
			}
		}
		
		function setErrorFor(input, message) {
			errors++;

			const formControl = input.parentElement;
			
			const small = formControl.querySelector("small");
			formControl.className = "form__item error";
			small.innerText = message;	
		}
		
		function setSuccessFor(input) {
			const formControl = input.parentElement;
			formControl.className = "form__item success";
		}
			
		const isEmail = (email) => {
			return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
		}

		const isPassword = password => {
			return /^\w{8,25}$/.test(password);
		}
    }
    
    checkForm ();
}