window.onload = function() {
    const openings = [
		{
			name: "Наруто - Опенинг 3",
			link: "https://ruv.hotmo.org/song/62967143",
			path: "naruto-op3.mp3"
		},
		{
			name: "Клинок рассекающий демонов - Опенинг 1",
			path: "blade-slayer-op1.mp3"
		},
		{
			name: "Атака титанов - Опенинг 1",
			path: "attack-on-titan-op1.mp3"
		},
		{
			name: "Твоё имя - Опенинг 1",
			path: "your-name.mp3"
		},
		{
			name: "Обещанный Неверленд - Опенинг 1",
			path: "promised-neverland-op1.mp3"
		},
		{
			name: "Город, в котором меня нет - Опенинг 1",
			path: "erased.mp3"
		},
	];
	
	const adjectives = [
		'целеустремленным',
		'проницательным',
		'сострадательным',
		'бесстрашным',
		'чистоплотным',
		'cмышленым'
	];
	
	const names = [
		'Наруто',
		'Изуко',
		'Танджиро',
		'Гатс',
		'Леви',
		'Эл'
	];    

	// Рандомное число
	const getRandomNumber = (min, max) => Math.floor(Math.random() * (max - min)) + min;

	// Вставить после
	const insertAfter = (elem, refElem)  => refElem.parentNode.insertBefore(elem, refElem.nextSibling);

	// Плеер
	function player () {
		const btnMusic = document.querySelector(".player__btn-play");
		const btnRandomMusic = document.querySelector(".player__btn");
		const musicName = document.querySelector(".player__name");
		const musicLink = document.querySelector(".player__link");

		let lastRandomNum;
		let currentRandomNum;

		const audio = new Audio(); 

		btnMusic.addEventListener("click", function(){
			btnMusic.classList.toggle("player__btn-play_active");

			if (btnMusic.classList.contains("player__btn-play_active")) {
				playPlayer();
			}
			else {
				pausePlayer();
			}
		});

		btnRandomMusic.addEventListener("click", function(e){
			e.preventDefault();

			btnMusic.classList.add("player__btn-play_active");

			startStatePlayer();

			playPlayer();
		});

		audio.addEventListener("ended", function(){
			btnMusic.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#play");
			btnMusic.classList.remove("player__btn-play_active");
		});

		startStatePlayer();

		function startStatePlayer () {
			currentRandomNum = getRandomNumber(0, openings.length);

			if (currentRandomNum === lastRandomNum) {
				startStatePlayer();
			}

			lastRandomNum = currentRandomNum;

			musicLink.href = "openings/" + openings[currentRandomNum]["path"];
			musicName.innerHTML = openings[currentRandomNum]["name"];
			audio.src = "openings/" + openings[currentRandomNum]["path"];
		}

		function playPlayer () {
			btnMusic.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#pause");
			audio.play();
		}

		function pausePlayer () {
			btnMusic.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#play");
			audio.pause();	
		}
	}

	// Запрет перетаскивания картинок
	function preventDragImg () {
		document.querySelectorAll("img").forEach(img => {
			img.addEventListener("dragstart", function(e){
				e.preventDefault();
			});
		});
	}

	// Получить информацию о рецензиях
	function getListItems () {
		document.querySelectorAll('.list__about').forEach(item => {
			item.addEventListener("click", function (e) {
			e.preventDefault();

			const preloader = document.querySelector(".preloader").classList;

			preloader.remove("js-hide");

			fetch(`vendor/post.php?id=${this.dataset.itemId}`)
				.then(response => response.json())
				.then(data => {
					
						const {listName, comment, rating, poster, date} = data;

						document.querySelector(".popup__title").innerHTML = listName;
						document.querySelector(".popup__text").innerHTML = marked(comment);
						document.querySelector(".popup__grade").innerText = rating;
						document.querySelector(".popup__poster").src = poster;
						document.querySelector(".popup__time").innerText = date;

						setTimeout(() => {
							preloader.add("js-hide");
						}, 5000);
				});
			}); 
		});
	}

	// Показать модальное окно
	function popup () {
		const popupBtnsShow =  document.querySelectorAll(".btn-popup-show");
		const popupBtnsHide = document.querySelectorAll(".btn-popup-hide");
		const popups = document.querySelectorAll(".popup");


		popupBtnsShow.forEach(btnShow => {
			btnShow.addEventListener("click", function (e) { 
				if (e.target.dataset.callPopup === "popup") {
					document.querySelector(`${this.dataset.popup}`).classList.remove("js-popup-hide");
					document.body.classList.add('fixed');
				}
			});
		})
	
		popupBtnsHide.forEach(btnHide => {
			btnHide.addEventListener("click", function(e){
				if (e.target.classList.contains("btn-popup-hide")) {
					popups.forEach(popup => {
						popup.classList.add("js-popup-hide");
						document.body.classList.remove('fixed');
					});
				}
			})
		})
	}

	// поиск в списке
	function searchMatches () {
		const input = document.querySelector("#search");
		const listNames = document.querySelectorAll(".list__name");
		const warning = document.querySelector(".list__warning");

		const regexp = /\s+/g;

		input.addEventListener("input", function (){
			let inputVal = this.value;
	
			let mismatches = 0;

			if (inputVal) {
				listNames.forEach(listName => {
					let item = listName.innerText.replace(regexp, "").toLowerCase();
					let match = inputVal.replace(regexp, "").toLowerCase();

					if (item.search(match) === -1) {
						listName.parentNode.classList.add("js-hide");
						mismatches++;

						if (mismatches === listNames.length) {
							warning.style.display = "block";
						}
					}
					else {
						listName.parentNode.classList.remove("js-hide");
						warning.style.display = "none";
					}
					
				});
			}
			else {
				listNames.forEach(itemList => {
					itemList.parentNode.classList.remove("js-hide");
					warning.style.display = "none";
				});
			}
		})
	}

	// Состояние списка
	function changeStateList () {
		const listItems = document.querySelectorAll(".list__item");
		let listTools = document.querySelector(".list__details");
		let listWarnig = document.querySelector(".list__warning");
		
		if (listItems.length === 0) {
			listTools.style.visibility = "hidden";
			listWarnig.style.display = "block";
		}
		else {
			listTools.style.visibility = "visible";
			listWarnig.style.display = "none";
		}
	}

	changeStateList ();

	// Cортировка
	function sortList () {
		document.querySelector(".list__btn_asc").addEventListener("click", function (){
			sortListAscAndDesk ("desk");
		});

		document.querySelector(".list__btn_desk ").addEventListener("click", function (){
			sortListAscAndDesk ();
		});

		function sortListAscAndDesk (sort) {
			
			const list = document.querySelector(".list__holder");
	
			for (let i = 0; i < list.children.length; i++) {
				for (let j = i; j < list.children.length; j++) {

					let firstItem = list.children[i].dataset.listName.toLowerCase();
					let secondItem = list.children[j].dataset.listName.toLowerCase();

					if (sort === "desk" ? firstItem > secondItem : firstItem < secondItem) {
						let replacedNode = list.replaceChild(list.children[j], list.children[i]);
						insertAfter(replacedNode, list.children[i]);
					}
				}
			}
		}
	}
    
    preventDragImg();
	sortList();
	searchMatches();
	player();
	popup();
	getListItems();
}